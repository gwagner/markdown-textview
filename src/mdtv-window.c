/* markdown_textview-window.c
 *
 * Copyright 2020 Günther Wagner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mdtv-config.h"
#include "mdtv-window.h"
#include "mdtv-buffer.h"
#include <cmark.h>

struct _MdtvWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkTextView         *textview;
};

G_DEFINE_TYPE (MdtvWindow, mdtv_window, GTK_TYPE_APPLICATION_WINDOW)

static void
mdtv_window_class_init (MdtvWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/MarkdownTextview/mdtv-window.ui");
  gtk_widget_class_bind_template_child (widget_class, MdtvWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, MdtvWindow, textview);
}

static void
mdtv_window_init (MdtvWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));


  /* gtk_text_buffer_create_tag (buffer, "code-block", "family", "Monospace", NULL); */
  /* gtk_text_buffer_create_tag (buffer, "code", "family", "Monospace", "background", "black", NULL); */
  /* gtk_text_buffer_create_tag (buffer, "invisible", "invisible", TRUE, NULL); */
  /* gtk_text_buffer_create_tag (buffer, "line", "justification", GTK_JUSTIFY_CENTER, NULL); */
  /* gtk_text_buffer_create_tag (buffer, "break", "background", "lightgrey", NULL); */

  gchar *markdown = "```rust\ncore::iter::traits::iterator::Iterator\n```\n\n```rust\nfn collect<B: FromIterator<Self::Item>>(self) -> B\nwhere Self: Sized\n```\n___\n\nTransforms an iterator into a collection.\n\n`collect()` can take anything iterable, and turn it into a relevant\ncollection. This is one of the more powerful methods in the standard\nlibrary, used in a variety of contexts.\n\nThe most basic pattern in which `collect()` is used is to turn one\ncollection into another. You take a collection, call [`iter`] on it,\ndo a bunch of transformations, and then `collect()` at the end.\n\nOne of the keys to `collect()`'s power is that many things you might\nnot think of as 'collections' actually are. For example, a [`String`]\nis a collection of [`char`]s. And a collection of\n[`Result<T, E>`][`Result`] can be thought of as single\n[`Result`]`<Collection<T>, E>`. See the examples below for more.\n\nBecause `collect()` is so general, it can cause problems with type\ninference. As such, `collect()` is one of the few times you'll see\nthe syntax affectionately known as the 'turbofish': `::<>`. This\nhelps the inference algorithm understand specifically which collection\nyou're trying to collect into.\n\n# Examples\n\nBasic usage:\n\n```\nlet a = [1, 2, 3];\n\nlet doubled: Vec<i32> = a.iter()\n                         .map(|&x| x * 2)\n                         .collect();\n\nassert_eq!(vec![2, 4, 6], doubled);\n```\n\nNote that we needed the `: Vec<i32>` on the left-hand side. This is because\nwe could collect into, for example, a [`VecDeque<T>`] instead:\n\n[`VecDeque<T>`]: ../../std/collections/struct.VecDeque.html\n\n```\nuse std::collections::VecDeque;\n\nlet a = [1, 2, 3];\n\nlet doubled: VecDeque<i32> = a.iter().map(|&x| x * 2).collect();\n\nassert_eq!(2, doubled[0]);\nassert_eq!(4, doubled[1]);\nassert_eq!(6, doubled[2]);\n```\n\nUsing the 'turbofish' instead of annotating `doubled`:\n\n```\nlet a = [1, 2, 3];\n\nlet doubled = a.iter().map(|x| x * 2).collect::<Vec<i32>>();\n\nassert_eq!(vec![2, 4, 6], doubled);\n```\n\nBecause `collect()` only cares about what you're collecting into, you can\nstill use a partial type hint, `_`, with the turbofish:\n\n```\nlet a = [1, 2, 3];\n\nlet doubled = a.iter().map(|x| x * 2).collect::<Vec<_>>();\n\nassert_eq!(vec![2, 4, 6], doubled);\n```\n\nUsing `collect()` to make a [`String`]:\n\n```\nlet chars = ['g', 'd', 'k', 'k', 'n'];\n\nlet hello: String = chars.iter()\n    .map(|&x| x as u8)\n    .map(|x| (x + 1) as char)\n    .collect();\n\nassert_eq!(\"hello\", hello);\n```\n\nIf you have a list of [`Result<T, E>`][`Result`]s, you can use `collect()` to\nsee if any of them failed:\n\n```\nlet results = [Ok(1), Err(\"nope\"), Ok(3), Err(\"bad\")];\n\nlet result: Result<Vec<_>, &str> = results.iter().cloned().collect();\n\n// gives us the first error\nassert_eq!(Err(\"nope\"), result);\n\nlet results = [Ok(1), Ok(3)];\n\nlet result: Result<Vec<_>, &str> = results.iter().cloned().collect();\n\n// gives us the list of answers\nassert_eq!(Ok(vec![1, 3]), result);\n```\n\n[`iter`]: ../../std/iter/trait.Iterator.html#tymethod.next\n[`String`]: ../../std/string/struct.String.html\n[`char`]: ../../std/primitive.char.html\n[`Result`]: ../../std/result/enum.Result.html";


  /* cmark_node *document = cmark_parse_document (markdown, strlen (markdown), CMARK_OPT_DEFAULT); */

  /* gchar *strbuf = cmark_render_commonmark (document, CMARK_OPT_DEFAULT, -1); */
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (mdtv_buffer_new (markdown));
  gtk_text_view_set_buffer (self->textview, buffer);

  /* GtkTextIter start, end; */
  /* gtk_text_buffer_get_start_iter (buffer, &start); */
  /* gtk_text_buffer_get_end_iter (buffer, &end); */
  /* g_autofree gchar *strbuf = gtk_text_buffer_get_text (buffer, &start, &end, TRUE); */

  /* cmark_node *document = cmark_parse_document (strbuf, strlen (strbuf), CMARK_OPT_DEFAULT); */
  /* cmark_iter *iter = cmark_iter_new (document); */
  /* iter = cmark_iter_new (document); */
  /* while (cmark_iter_next (iter) != CMARK_EVENT_DONE) */
  /*   { */
  /*     cmark_node *node = cmark_iter_get_node (iter); */
  /*     if (cmark_node_get_type (node) == CMARK_NODE_CODE_BLOCK) */
  /*       { */
  /*         GtkTextIter start, end; */
  /*         gtk_text_buffer_get_iter_at_line_offset (buffer, &start, cmark_node_get_start_line (node)-1, cmark_node_get_start_column (node)-1); */
  /*         gtk_text_buffer_get_iter_at_line_offset (buffer, &end, cmark_node_get_end_line (node)-1, cmark_node_get_end_column (node)); */
  /*         gtk_text_buffer_apply_tag_by_name (buffer, "code-block", &start, &end); */

  /*         GtkTextIter tmp = start; */
  /*         gtk_text_iter_forward_line (&tmp); */
  /*         gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &start, &tmp); */

  /*         tmp = end; */
  /*         GtkTextIter tmp2 = end; */
  /*         gtk_text_iter_forward_line (&tmp2); */
  /*         gtk_text_iter_set_line (&tmp, gtk_text_iter_get_line (&end)); */
  /*         gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &tmp, &tmp2); */
  /*       } */
  /*     else if (cmark_node_get_type (node) == CMARK_NODE_CODE) */
  /*       { */
  /*         GtkTextIter start, end; */
  /*         gtk_text_buffer_get_iter_at_line_offset (buffer, &start, cmark_node_get_start_line (node)-1, cmark_node_get_start_column (node)-1); */
  /*         gtk_text_buffer_get_iter_at_line_offset (buffer, &end, cmark_node_get_end_line (node)-1, cmark_node_get_end_column (node)); */
  /*         gtk_text_buffer_apply_tag_by_name (buffer, "code", &start, &end); */

  /*         GtkTextIter tmp = start; */
  /*         gtk_text_iter_backward_char (&tmp); */
  /*         gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &tmp, &start); */

  /*         tmp = end; */
  /*         gtk_text_iter_forward_char (&tmp); */
  /*         gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &end, &tmp); */
  /*       } */
  /*     else if (cmark_node_get_type (node) == CMARK_NODE_HRULE) */
  /*       { */
  /*         g_print ("%d %d\n%d %d\n---\n", cmark_node_get_start_line (node), cmark_node_get_start_column (node), cmark_node_get_end_line (node), cmark_node_get_end_column (node)); */
  /*         GtkTextIter start, end; */
  /*         gtk_text_buffer_get_iter_at_line_offset (buffer, &start, cmark_node_get_start_line (node)-1, cmark_node_get_start_column (node)-1); */
  /*         gtk_text_buffer_get_iter_at_line_offset (buffer, &end, cmark_node_get_end_line (node)-1, cmark_node_get_end_column (node)); */
  /*         gtk_text_buffer_apply_tag_by_name (buffer, "line", &start, &end); */
  /*       } */
  /*   } */
}
