/* mdtv-buffer.c
 *
 * Copyright 2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "mdtv-buffer.h"
#include <cmark.h>

struct _MdtvBuffer
{
  GtkTextBuffer parent_instance;
};

G_DEFINE_TYPE (MdtvBuffer, mdtv_buffer, GTK_TYPE_TEXT_BUFFER)

MdtvBuffer *
mdtv_buffer_new (gchar * content)
{
  cmark_node *document = NULL;
  g_autofree gchar *content_sanitized = NULL;
  GtkTextTagTable *table = NULL;

  /**
   * Sanitize content
   */
  document = cmark_parse_document (content, strlen (content), CMARK_OPT_DEFAULT);
  content_sanitized = cmark_render_commonmark (document, CMARK_OPT_DEFAULT, -1);
  cmark_node_free (document);

  table = gtk_text_tag_table_new ();

  return g_object_new (MDTV_TYPE_BUFFER, "text", content_sanitized, "tag-table", table, NULL);
}

static void
mdtv_buffer_notify (GObject    *object,
                    GParamSpec *pspec)
{
  G_OBJECT_CLASS (mdtv_buffer_parent_class)->notify (object, pspec);

  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (object);

  if (g_strcmp0 (g_param_spec_get_name (pspec), "text") == 0)
    {
      GtkTextIter start, end;
      gtk_text_buffer_get_start_iter (buffer, &start);
      gtk_text_buffer_get_end_iter (buffer, &end);
      g_autofree gchar *strbuf = gtk_text_buffer_get_text (buffer, &start, &end, TRUE);

      cmark_node *document = cmark_parse_document (strbuf, strlen (strbuf), CMARK_OPT_DEFAULT);
      cmark_iter *iter = cmark_iter_new (document);
      iter = cmark_iter_new (document);
      while (cmark_iter_next (iter) != CMARK_EVENT_DONE)
        {
          cmark_node *node = cmark_iter_get_node (iter);
          if (cmark_node_get_type (node) == CMARK_NODE_CODE_BLOCK)
            {
              GtkTextIter start, end;
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &start,
                                                       cmark_node_get_start_line (node) - 1,
                                                       cmark_node_get_start_column (node) - 1);
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &end,
                                                       cmark_node_get_end_line (node) - 1,
                                                       cmark_node_get_end_column (node));
              gtk_text_buffer_apply_tag_by_name (buffer, "code-block", &start, &end);

              GtkTextIter tmp = start;
              gtk_text_iter_forward_line (&tmp);
              gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &start, &tmp);

              tmp = end;
              GtkTextIter tmp2 = end;
              gtk_text_iter_forward_line (&tmp2);
              gtk_text_iter_set_line (&tmp, gtk_text_iter_get_line (&end));
              gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &tmp, &tmp2);
            }
          else if (cmark_node_get_type (node) == CMARK_NODE_CODE)
            {
              GtkTextIter start, end;
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &start,
                                                       cmark_node_get_start_line (node) - 1,
                                                       cmark_node_get_start_column (node) - 1);
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &end,
                                                       cmark_node_get_end_line (node) - 1,
                                                       cmark_node_get_end_column (node));
              gtk_text_buffer_apply_tag_by_name (buffer, "code", &start, &end);

              GtkTextIter tmp = start;
              gtk_text_iter_backward_char (&tmp);
              gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &tmp, &start);

              tmp = end;
              gtk_text_iter_forward_char (&tmp);
              gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &end, &tmp);
            }
          else if (cmark_node_get_type (node) == CMARK_NODE_HRULE)
            {
              g_print ("%d %d\n%d %d\n---\n",
                       cmark_node_get_start_line (node),
                       cmark_node_get_start_column (node),
                       cmark_node_get_end_line (node),
                       cmark_node_get_end_column (node));
              GtkTextIter start, end;
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &start,
                                                       cmark_node_get_start_line (node) - 1,
                                                       cmark_node_get_start_column (node) - 1);
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &end,
                                                       cmark_node_get_end_line (node) - 1,
                                                       cmark_node_get_end_column (node));
              gtk_text_buffer_apply_tag_by_name (buffer, "line", &start, &end);
            }
          else if (cmark_node_get_type (node) == CMARK_NODE_HEADING)
            {
              GtkTextIter start, end;
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &start,
                                                       cmark_node_get_start_line (node) - 1,
                                                       cmark_node_get_start_column (node) - 1);
              gtk_text_buffer_get_iter_at_line_offset (buffer,
                                                       &end,
                                                       cmark_node_get_end_line (node) - 1,
                                                       cmark_node_get_end_column (node));

              gtk_text_buffer_apply_tag_by_name (buffer, "heading", &start, &end);

              // remove markup
              GtkTextIter tmp = start;
              gtk_text_iter_forward_chars (&tmp, 2);
              gtk_text_buffer_apply_tag_by_name (buffer, "invisible", &start, &tmp);
            }
        }
    }
}

static void
mdtv_buffer_constructed (GObject *object)
{
  MdtvBuffer *self = (MdtvBuffer *)object;

  G_OBJECT_CLASS (mdtv_buffer_parent_class)->constructed (object);

  gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (self), "code-block", "family", "Monospace", NULL);
  gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (self), "code", "family", "Monospace", "background", "black", NULL);
  gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (self), "invisible", "invisible", TRUE, NULL);
  gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (self), "line", "justification", GTK_JUSTIFY_CENTER, NULL);
  gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (self), "heading", "scale", PANGO_SCALE_LARGE, NULL);
  gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (self), "break", "background", "lightgrey", NULL);
}

static void
mdtv_buffer_finalize (GObject *object)
{
  MdtvBuffer *self = (MdtvBuffer *)object;

  G_OBJECT_CLASS (mdtv_buffer_parent_class)->finalize (object);
}

static void
mdtv_buffer_class_init (MdtvBufferClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mdtv_buffer_finalize;
  object_class->constructed = mdtv_buffer_constructed;
  object_class->notify = mdtv_buffer_notify;
}

static void
mdtv_buffer_init (MdtvBuffer *self)
{
}
